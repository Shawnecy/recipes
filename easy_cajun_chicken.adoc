= Easy Cajun Chicken

== Ingredients

* 5 lbs chicken parts (wings or thighs are best)
* 2 tbsp olive oil
* ¼ C Cajun seasoning (store bought or use blend recipe below)
* Ranch dressing for serving (optional)

== Cajun Seasoning Ingredients

* 2 tsp kosher salt
* 1 tbsp garlic powder
* 1 tsp black pepper
* 1 ½ tsp onion powder
* 1 tsp cayenne pepper
* 1 ½ tsp dried oregano
* 1 tsp dried thyme

== Directions

. Rub the chicken with olive oil.
. Add Cajun seasoning and toss until chicken is fully coated.
. Spread the chicken out on cookie sheets with at least an inch between pieces.
. Bake in the center of a preheated 400 degree (F) oven for about 50 minutes (40 minutes for wings).

== Notes

The skin should be golden and crispy, and the juices should run clear.

Makes 6 – 8 servings

From https://www.ibreatheimhungry.com/easy-cajun-chicken-low-carb-gluten-free/