= Nut Rolls

== Dough

=== Ingredients

* 2 small cakes fast acting yeast
* ½ C warm water
* 1 C sour cream
* ½ lb. butter
* 2 eggs, well beaten
* 1 C sugar
* ½ tsp salt
* 6 C flour

=== Directions

. Cream butter, add sugar slowly and cream well.
. Add eggs.
. Dissolve yeast in warm water.
. Add sour cream and salt to yeast water.
. Interleave adding flour and yeast mix to butter mixture.
. Chill 2-24 hours.


== Nut Filling

=== Ingredients

* 2 pounds walnuts
* 1-¼ C sugar
* ½ C milk
* 3 egg whites (beaten)

=== Directions

. Grind walnuts.
. Mix sugar, milk, egg whites, and walnuts into a paste.

== Final Steps

. Roll out dough into 4 pieces.
. Spread nut filling on dough.
. Let rise for 2 hours.
.. (Note: I don't see much rising even waiting for 2 hours)
. Bake 350 degrees F for 35 minutes.
..  (Note: the original value of 375 was overwritten)