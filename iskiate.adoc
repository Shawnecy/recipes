= Iskiate

== Ingredients

* 4 cups water
* 2 limes, juice only
* ¼ cup raw chia seeds
* ¼ cup agave nectar, honey, or sugar

== Directions

. Cook all ingredients a bit to break down the chia seeds some.
. Blend.